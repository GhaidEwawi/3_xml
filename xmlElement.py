import re
class XmlElement:
    def __init__(self,
                 name='default',
                 parent=None,
                 children=[],
                 attributes={},
                 text=None):
        self.name = name
        self.parent = None
        self.children = children
        self.attributes = attributes
        self.text = text

    def __str__(self):
        return self.name

    def print(self):
        print(f'<{self.name} ', ' '.join(f"{key}=\"{value}\"" for key, value in self.attributes.items()), '>', sep='')
        print(self.text)
        for child in self.children:
            child.print()
        print(f'</{self.name}>')

    def toString(self):
        str = ""
        str += '<' + self.name + ' '
        str += ' '.join(f"{key}=\"{value}\"" for key, value in self.attributes.items())
        str += '>'
        str += self.text
        str += '\n'
        for child in self.children:
            str += child.toString()
            str += '\n'
        str += '</' + self.name + '>'
        return str

    def readElement(self, st):

        self.children = []
        self.parent = None

        # Getting the opening tag
        pattern = re.compile(r'<\s?(\w+)(\s?\w+="\w+\")*\s?>')
        open = re.search(pattern, st)
        self.name = open.group(1)
        end = re.search(f'<\s?/\s?{self.name}\s?>', st)

        # Parsing the opening tag to find attributes
        self.attributes = self.readName(st[open.start(): open.end()])

        # Parsing the string to find the text
        st = st[open.end(): end.start()]
        self.text = XmlElement.readText(st)

        # Parsing the remaining string to find children
        index = 0
        while re.search(pattern, st[index:]) is not None:
            childBegin = re.search(pattern, st[index:])
            childName = childBegin.group(1)
            childEnd = re.search(f'<\s?/\s?{childName}\s?>', st)
            temp = XmlElement()
            temp.readElement(st[index+childBegin.start():childEnd.end()])
            temp.parent = self
            index = childEnd.end()
            self.children.append(temp)

        return self

    @staticmethod
    def readName(st):
        name = ""
        attributes = {}
        st = st.replace('\"', "")
        st = st.split(' ')

        for s in st:
            if s.find('<') > 0:
                name = re.split('<')[0]
            if s.find('=') > 0:
                s = s.split('=')
                attributes[s[0]] = s[1]
        return attributes

    @staticmethod
    def readText(st):
        text = re.search(r'([\w] ?)+', st)
        return st[text.start():text.end()]

    def printChildren(self):
        print('Level is ', "  children are ", len(self.children))
        for child in self.children:
            print(child.name)

    def details(self):
        print(f"name: {self.name}\ntext:{self.text}")
        print(f"children = {len(self.children)}", [child.text for child in self.children])
