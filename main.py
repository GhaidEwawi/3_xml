from xmlTree import XmlTree
from xmlElement import XmlElement
import os
import xml.etree.ElementTree as ET
import timeit

#test = {'one': 1, 'two': 2}
#child = [XmlElement()]
#something = XmlElement(attributes=test, children=child, text='text')
#something.print()
def avg(arr):
    sum = 0
    for n in arr:
        sum+=n

    return sum/len(arr)

def compare(loops, oneTest):
    times = []
    times2= []
    for i in range(loops):
        print(f'test number#{i}')
        start = timeit.default_timer()
        for i in range(oneTest):
            tree = XmlTree.createFromFile(os.getcwd() + '/note.txt')
        stop = timeit.default_timer()
        time = round(stop - start, 6)
        times.append(time)
        #tree.print()
        start = timeit.default_timer()
        for i in range(oneTest):
            tree = ET.TreeBuilder('note.txt')
        stop = timeit.default_timer()
        time = round(stop - start, 6)
        times2.append(time)
    return avg(times), avg(times2)

if __name__ == "__main__":
    #one, two = compare(50, 1000)
    #print(f'My time = {one}, ET time = {two}')
    tree = XmlTree.createFromFile(os.getcwd() + '/note.txt')
    tree.print()
    tree.saveToFile('new.txt')
