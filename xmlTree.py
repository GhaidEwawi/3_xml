import os, sys, re
from xmlElement import XmlElement

class XmlTree:
    """Contains an XmlTree

    Attributes:
        parent -- the parent node of the tree
        size -- number of the nodes that the tree contains, including the parent node
    """
    def __init__(self, parent=XmlElement(), size=0):
        self.parent = parent
        self.size = size

    @staticmethod
    def createFromFile(filePath):

        if os.path.isfile(filePath):
            try:
                f = open(filePath)
                s = f.read()
                temp = XmlElement().readElement(s)
                tempSize = len(re.findall(r'<\s?(\w+)(\s?\w+="\w+\")*\s?>.*()', s))
                tree = XmlTree(parent=temp, size=tempSize)
                return tree
            except Exception as myEx:
                print("Error: ", myEx)
        else:
            print("File does not exist")

    def saveToFile(self, path):
        try:
            f = open(path, 'w')
            s = self.parent.toString()
            f.write(s)
        except Exception as myEx:
            print("Error: ", myEx)

    def print(self):
        self.parent.print()
